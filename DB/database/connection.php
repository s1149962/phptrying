
<?php
/*
In this document we have our connection class, we use this to connect to our database if we ever want to update/delete/insert something
*/

class onnection // class connection which will handle everything related to the connection
{
    public static function make() // we make a function (aka method in OOP)
    {
        $hostname = '127.0.0.1';
        $username = 'root';
        $password = '';
        $dbname = "wfflix";

        try {
            $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password); // standard connection precedure
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // handles errors better if there are any
            return $dbh; // we make sure to return this, so we can grab this in another class (mainly used for Select.php)
        } catch (PDOException $e) { // error handeling
            echo $e->getMessage(); // we get the error message
        }
    }
}

