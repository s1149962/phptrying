<html>
<head>
    <title>DB test page</title>
    <meta charset="UTF-8">
</head>
<body>
<?php require "header.php" ?>

<div>
    <?php

    class TableRows extends RecursiveIteratorIterator {
        function __construct($it) {
            parent::__construct($it);
        }
    }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "test";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $stmt = $conn->prepare("SELECT id, first_name, last_name FROM test.teachers");
        $stmt->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
            echo $v;
        }
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;
    echo "</table>";
    ?>
</div>

</body>
</html>