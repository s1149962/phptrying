
OUTCOME:
<?php


class Calc2 {
    public $num1;
    public $num2;
    public $cal;
    public function __construct($box1, $box2, $choice) {
        $this->num1 = $box1;
        $this->num2 = $box2;
        $this->cal = $choice;
    }
    public function calcMethod() {
      switch ($this->cal) {
        case '+':
            $result = $this->num1 + $this->num2;
            break;
        case '-':
            $result = $this->num1 - $this->num2;
            break;
        case 'x':
            $result = $this->num1 * $this->num2;
            break;
          case ':':
              $result = $this->num1 / $this->num2;
              break;
        default:
            $result = "Error";
            break;
      }
      return $result;
    }
}
 ?>

<?php

$num1 = $_POST['box1'];
$num2 = $_POST['box2'];
$cal = $_POST['choice'];
$calculator = new Calc2($num1, $num2, $cal);
echo $calculator->calcMethod();

?>

<html lang="en">
<head><title>OUTCOME</title></head>
<body>
<br>
<a href="/index.php"> <p>Terug Naar HomePage</p>
</a>
</body>
</html>
