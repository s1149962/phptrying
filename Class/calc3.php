
OUTCOME:
<?php


class calc3
{

    public $nm1;
    public $nm2;
    public $choices;
    public function __construct($n1, $n2, $c) {
        $this->nm1 = $n1;
        $this->nm2 = $n2;
        $this->choices = $c;
    }
    public function calcMethod() {
        switch ($this->choices) {
            case '+':
                $result = $this->nm1 + $this->nm2;
                break;
            case '-':
                $result = $this->nm1 - $this->nm2;
                break;
            case 'x':
                $result = $this->nm1 * $this->nm2;
                break;
            case ':':
                $result = $this->nm1 / $this->nm2;
                break;

        default:
            $result = "Error";
            break;
      }
      return $result;
    }
}

?>

<?php

$nm1 = $_POST['n1'];
$nm2 = $_POST['n2'];
$choices = $_POST['c'];
$calculator = new Calc3($nm1, $nm2, $choices);
echo $calculator->calcMethod();

?>

<html lang="en">
<head><title>OUTCOME</title></head>
<body>
<br>
<a href="/index.php"> <p>Terug Naar HomePage</p>
</a>
</body>
</html>


