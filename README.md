# gitlearning

learning git
<p> use this file to keep information you learn </p>

* git clone plakken. voorbeeld terminal= git clone https://gitlab.com/s1149962/gitlearning.git
* files aanmaken: rechterklik file, type naam bestand plus de type . bestand. voorbeeld csharp.txt
* terminal: git add (name file) zorgt dat file getracked word. git add . doet allen
* terminal: git status bekijkt de status
* terminal: dir   bekijkt welke bestanden er zijn in de directory waar je in zit
* terminal: cd (name file/map) brengt je naar die dir
* terminal: git commit -m "bestanden aangemaakt voor project" geeft aan wat je hebt gedaan
* terminal: git push   stuurd het naar git
* als je wachtwoord verkeerd typed: control panel/user acounts/manage windows credentials  verwijder git account
* terminal: git pull   om updated versie van git te halen
* terminal: git branch (csharp)
* terminal: git checkout (csharp)
* terminal: php -S localhost:****  om php server te starten
