<html>
<head>
    <title>DB test page</title>
    <meta charset="UTF-8">
</head>
<body>
    <?php

    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "test";


    class TableRows extends RecursiveIteratorIterator {
        function __construct($it) {
            parent::__construct($it);
        }
    }
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $stmt = $conn->prepare("SELECT id, first_name, last_name FROM test.teachers");
        $stmt->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
            echo $v ;
        }
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;
    echo "</table>";
    ?>
</div>

<div>


</div>

</body>
</html>